let s:neovimrc_tagbar_bin_location = '~/.vim/' .
            \ 'plugged/markdown2ctags/markdown2ctags.py'
let s:neovimrc_tagbar_type_markdown = {
            \ 'ctagstype': 'markdown',
            \ 'ctagsbin' : s:neovimrc_tagbar_bin_location,
            \ 'ctagsargs' : '-f - --sort=yes',
            \ 'kinds' : [
            \ 's:sections',
            \ 'i:images'
            \ ],
            \ 'sro' : '|',
            \ 'kind2scope' : {
            \ 's' : 'section',
            \ },
            \ 'sort': 0,
            \ }
let g:tagbar_type_markdown = s:neovimrc_tagbar_type_markdown
let g:tagbar_type_mkd = s:neovimrc_tagbar_type_markdown
