let g:airline_powerline_fonts=g:neovimrc_patched_icons_loaded

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#tabline#excludes = ['vimfiler:explorer', 'Result']

let g:airline#extensions#whitespace#checks=[]

let g:airline#extensions#tagbar#enabled = 0
au FileType php,javascript,java,markdown let g:airline#extensions#tagbar#enabled = 1
let g:airline#extensions#tagbar#flags = 'f'

let g:airline#extensions#tmuxline#enabled = 1
