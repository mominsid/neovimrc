let g:airline_theme = 'lucius'

" I want to run  some small customizations that make Lucius
" prettier. So, I'll make a command that does all that.
" one argument is optional - it is the command for a specific colorscheme
" variation: e.g. LuciusDark, LuciusBlack, LuciusBlackLowContrast, etc.
function! s:initLucius(luciusCommand)
    let c = a:luciusCommand
    colorscheme lucius
    " If the argument is empty or isn't one of the expected commands
    " just use a default
    if c == ''
                \|| exists(':' . c) != 2
                \|| match(c, '^Lucius[BD]') == -1
        let c = 'LuciusBlackLowContrast'
    endif

    exec c
    " Use better colors for the numberline
    " The black themes will also benefit from a lighter color
    if match(c, '^LuciusB') != -1
        highlight LineNr ctermbg=235
        highlight SignColumn ctermbg=235
        highlight GitGutterAdd ctermbg=235
        highlight GitGutterChange ctermbg=235
        highlight GitGutterDelete ctermbg=235
        highlight GitGutterChangeDelete ctermbg=235
    else
    endif
    hi CursorLineNr ctermbg=24 ctermfg=253

    " Force Green for cursor, and then obviate the need for
    " CursorLine and CursorColumn
    hi Cursor ctermfg=28
    set nocursorcolumn
    set nocursorline
endfunction
command! -nargs=? NeoVimrcInitLucius call s:initLucius(<q-args>)

" For vim airline, if we are using Lucius as colorscheme
" Let's make the normal color palette more interesting
"  Use blues
if g:airline_theme == 'lucius'
    let g:airline_theme_patch_func = 'LuciusAirlineThemePatch'
    function! LuciusAirlineThemePatch(palette)
        "let s:N1 = airline#themes#get_highlight('StatusLine')
        let s:N1 = airline#themes#get_highlight('TabLineSel')
        let s:N2 = airline#themes#get_highlight('Folded')
        let s:N3 = airline#themes#get_highlight('LineNr')
        let a:palette.normal = airline#themes#generate_color_map
                    \(s:N1, s:N2, s:N3)
    endfunction
endif
