let g:dbext_default_prompt_for_parameters=0
let g:dbext_default_buffer_lines = 5
nnoremap<leader>d :DBExecRangeSQL<cr>
vnoremap<leader>d :DBExecRangeSQL<cr>
