" Style and strict checkers go haywire in an unorganized project
let g:syntastic_php_checkers = ['php']
let g:syntastic_javascript_eslint_exe='"$(npm bin)/eslint"'
let g:syntastic_javascript_checkers = ['eslint']

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_wq = 0

"let g:syntastic_html_tidy_quiet_messages = { "level" : "warnings" }
let g:syntastic_quiet_messages = {"regex": '\.*\(or unknown entity\|proprietary attribute\|<custom> is not recognized\).*'}
