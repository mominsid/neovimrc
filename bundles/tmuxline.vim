let g:tmuxline_preset = {
            \'a'    : '#S',
            \'b'    : '#(whoami)',
            \'win'  : '#I #W',
            \'cwin' : '#I #W',
            \'y'    : '#(date +"%a %Y-%m-%d %H:%M")',
            \'z'    : '#H'
            \}
