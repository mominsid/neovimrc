" Customize fzf colors to match your color scheme

nmap <Space> [fzf]
nnoremap [fzf] <Nop>
nnoremap <silent> [fzf]p :Files<CR>
nnoremap [fzf]/ :Ag 
nnoremap <silent> [fzf]m :Marks<CR>
nnoremap <silent> [fzf]f :FZFMru<CR>
nnoremap <silent> [fzf]b :Buffers<CR>
nnoremap <silent> [fzf]t :Tags<CR>
nnoremap <silent> [fzf]c :Commands<CR>

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

let g:fzf_nvim_statusline = 1

if executable('rg')
    set grepprg=rg\ --vimgrep
    command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)
    nnoremap [fzf]/ :Rg 
endif
