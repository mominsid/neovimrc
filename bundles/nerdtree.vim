autocmd FileType nerdtree setlocal nu rnu

nnoremap <C-T> :NERDTreeToggle<CR>
