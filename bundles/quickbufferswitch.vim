au FileType nerdtree nnoremap <buffer> <s-h> <nop>
au FileType tagbar nnoremap <buffer> <s-h> <nop>
au Filetype vimfiler nnoremap <buffer> <s-l> <nop>
au FileType nerdtree nnoremap <buffer> <s-l> <nop>
au FileType tagbar nnoremap <buffer> <s-l> <nop>
au Filetype vimfiler nnoremap <buffer> <s-h> <nop>
nnoremap <silent> <s-l> :Bnext<cr>
nnoremap <silent> <s-h> :BNext<cr>
nnoremap <C-w><C-w> :QBdelete<CR>
