" EDIT: now using simeji/winresizer. Start with <c-e>, then use hjkl and <Enter>
" remap arrow keys
"nnoremap <up> <C-w>+
"nnoremap <down> <C-w>-
"nnoremap <left> 4<C-w><
"nnoremap <right> 4<C-w>>


" In normal terminals, escape enters normal mode (<c-\><c-n>), but this
" is a problem in fzf, where escaping also closes the process. Leaving
" the fzf buffer without closing the process will cause a runaway process
" it is bad
function! Neovimrc_remap_escape_terminal()
    let l:filetype = &filetype
    if (l:filetype != 'fzf')
    else
        call feedkeys("i\<C-C>")
    endif
endfunction
"tnoremap <silent> <esc> <c-\><c-n>:call Neovimrc_remap_escape_terminal()<CR>
tnoremap <silent> <c-\><c-n> <c-\><c-n>:call Neovimrc_remap_escape_terminal()<CR>

set rnu
set nu
set noundofile
set undoreload=0                                    "Reloading (or external change of) a file resets the undo buffers
"set mouse=a                                         "enable mouse
set mousehide                                       "hide when characters are typed
set history=1000                                    "number of command lines to remember
set ttyfast                                         "assume fast terminal connection
set viewoptions=folds,options,cursor,unix,slash     "unix/windows compatibility
set encoding=utf-8                                  "set encoding for text
set hidden                                          "allow buffer switching without saving
set autoread                                        "auto reload if file saved externally
set nrformats-=octal                                "always assume decimal numbers
set showfulltag
set backspace=indent,eol,start                      "allow backspacing everything in insert mode
set nospell

set autoindent                                      "automatically indent to match adjacent lines
set expandtab                                       "spaces instead of tabs
set smarttab                                        "use shiftwidth to enter tabs
set shiftwidth=4
set tabstop=4
set softtabstop=4
"autocmd filetype javascript.jsx setlocal shiftwidth=2
"autocmd filetype javascript.jsx setlocal tabstop=2
"autocmd filetype javascript.jsx setlocal softtabstop=2

set list                                            "highlight whitespace
set listchars=tab:│\ ,trail:•
set shiftround
set linebreak
" let &showbreak='↪ '
set scrolloff=1                                     "always show content after scroll
set scrolljump=5                                    "minimum number of lines to scroll
set display+=lastline
set wildmenu                                        "show list for autocomplete
set wildmode=list:full
set wildignorecase
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store
set completeopt=menuone,noselect
"set completeopt=preview,menuone,noselect
set splitbelow
set splitright
set noerrorbells
set novisualbell
set t_vb=
set hlsearch                                        "highlight searches
set incsearch                                       "incremental searching
set ignorecase                                      "ignore case for searching
set smartcase                                       "do case-sensitive if there's a capital letter
" swap files
set directory=~/.vim/.cache/swap
set noswapfile
set wrap
set cursorline
set cursorcolumn

" FOLD STUFF
" Simple and often effective way to fold.
" I have found that more advanced ways, like syntax, can sometimes
" get slow
set foldmethod=manual
autocmd FileType html,xhtml,xml nnoremap zc zfit
autocmd FileType php,javascript nnoremap zc zfi{

" PHP STUFF
let php_htmlInStrings = 1  "Syntax highlight HTML code inside PHP strings.
let php_sql_query = 1      "Syntax highlight SQL code inside PHP strings.
let php_noShortTags = 1    "Disable PHP short tags.

" Note that vim isn't aware of the csv file type without a plugin
" If this becomes an issue, use file extension for csv instead?
autocmd FileType csv,xml set nowrap
autocmd BufEnter,BufNew *.csv set nowrap

set laststatus=2

" Get rid of gvim toolbars and scrollbars
set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=L

set tags=./tags;

" Per-directory vimrc for projects. And disable unsafe commands in local
" vimrc
set exrc
set secure

syntax enable


set background=dark

autocmd FileType smarty set filetype=smarty.html

if $COLORTERM == 'gnome-terminal' || $TERM == 'screen-256color'
    set t_Co=256
endif

function! s:toggle_colorcolumn(col)
    let col = a:col
    let cur = &colorcolumn
    if col == cur
        set colorcolumn=col
    else
        set colorcolumn=
    endif
endfunction

let s:set_column = ''
function! s:toggle_colorcolumn()
    let cur = &colorcolumn
    if cur == ''
        if s:set_column == ''
            let s:set_column = g:neovimrc_colorcolumn_column
        endif
        execute 'set colorcolumn=' .
                    \ s:set_column
    else
        let s:set_column = cur
        set colorcolumn=
    endif
endfunction


let s:default_column = 81
let g:neovimrc_colorcolumn_column =
            \ get(g:, 'neovimrc_colorcolumn_column', s:default_column)
command! NeovimrcToggleColorColumn call
            \ s:toggle_colorcolumn()
nnoremap <leader>l :NeovimrcToggleColorColumn<CR>

" In Windows WSL, if you use the excellent
" https://github.com/goreliu/wsl-terminal/wiki/Tips, you can get access to the
" clipboard. In this configuration, use Leader-c while in visual mode to copy
" a selection. Or use Leader-a-c in normal mode to copy an entire file
if executable('wrun')
    vnoremap <silent> <Leader>c :<C-U>silent :'<,'>:w !wrun clip<CR>:echomsg 'Selection Copied'<CR>
    nnoremap <silent> <Leader>ac :silent !cat % \| wrun clip<CR>:echomsg 'File Copied'<CR>
endif
