let $NVIM_TUI_ENABLE_TRUE_COLOR=1
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

let g:dbext_default_profile_mysql_local='type=MYSQL:user=USERNAME:passwd=PASSWORD'
let g:dbext_default_profile = 'mysql_local'

let g:neovimrc_patched_icons_loaded=0
" Set to true if you are using Windows Subsystem for Linux
let g:neovimrc_is_wsl = 0

source /home/vagrant/neovimrc/dotvimrc

" Set lucius as the colorscheme using a custom commandof mine
NeoVimrcInitLucius LuciusBlackLowContrast

" System-specific mouse use
set mouse=
